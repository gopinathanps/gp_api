<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Loan;
use App\Http\Resources\LoanResource;
use Validator;
use DB;
use Carbon\Carbon;
class LoanRepaymnetController extends Controller
{
    /**
     * Update a loan payment.
     *
     * @return \Illuminate\Http\Response
     */
    public function pay(Request $request,$loan_id)
    {
    	$amount = $request->amount;
        $loan_payment = DB::table('loan_repayment')->select(DB::raw('MAX(created_date) as latest_date'), DB::raw('SUM(loan_repayment.amount) as paid_total_amount'))->where('loan_id', $loan_id)->first();
       $loan = DB::table('loans')->select('amount as loan_amount')->where('id', $loan_id)->first();
        $message = "Nothing to Update";

        if($loan_payment)
        {
        	//Checking repayemnt amount
        	if($loan_payment->paid_total_amount < $loan->loan_amount )
			{
				//Checking payemnt date difference to repayment
	        	$now = Carbon::now();
	        	$last_payment_on = $loan_payment->latest_date;
				$last_payment_diff = $now->diffInDays($last_payment_on);
				if($last_payment_diff >7 )
				{
					DB::table('loan_repayment')->insert([
						'loan_id' => $loan_id,
					    'amount' => $amount,
					    'created_date' => $now
					]);
					$message = "Paid successfully";

				}
				else
				{
					$message = "Payemnt only in weekly";

				}

			}
			else
			{
				$message = "Loan amount paid back";

			}



        }
        return response(['message' => $message], 200);
    }
}
