<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Loan;
use App\Http\Resources\LoanResource;
use Validator;

class LoanController extends Controller
{
    /**
     * Display a listing of the loans.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $loan = Loan::all();
        return response([ 'loans' => LoanResource::collection($loan), 'message' => 'Retrieved successfully'], 200);
    }

    /**
     * Store a newly created loan.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $validator = Validator::make($data, [
            'customer_id' => 'required',
            'term' => 'required',
            'amount' => 'required',
            'status' => 'required',
        ]);

        if($validator->fails()){
            return response(['error' => $validator->errors(), 'Validation Error']);
        }

        $loan = Loan::create($data);

        return response([ 'loan' => new LoanResource($loan), 'message' => 'Created successfully'], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Loan  $loan
     * @return \Illuminate\Http\Response
     */
    public function show(Loan $loan)
    {
        return response([ 'loan' => new LoanResource($loan), 'message' => 'Retrieved successfully'], 200);

    }

    /**
     * Update the specified resource in Loan.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Loan  $loan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Loan $loan)
    {

        $loan->update($request->all());

        return response([ 'loan' => new LoanResource($loan), 'message' => 'Retrieved successfully'], 200);
    }

    /**
     * Remove the specified resource from Loan.
     *
     * @param \App\Loan $loan
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Loan $loan)
    {
        $loan->delete();

        return response(['message' => 'Deleted']);
    }
}
